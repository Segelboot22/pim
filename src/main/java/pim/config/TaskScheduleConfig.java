package pim.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import pim.model.repository.CarRepository;

import java.util.Calendar;

@Configuration
public class TaskScheduleConfig {

    private final CarRepository carRepository;

    public TaskScheduleConfig(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

//    @Scheduled(cron = "1 * * * * *")
//    public void deleteAll() {
//
//        Calendar instance = Calendar.getInstance();
//        instance.set(2018, Calendar.JULY, 15);
//
//        carRepository.deleteAllByDateBefore(instance.getTime());
//    }
}
