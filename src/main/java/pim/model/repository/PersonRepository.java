package pim.model.repository;

import org.springframework.data.repository.CrudRepository;
import pim.model.entity.Person;

import java.util.List;

public interface PersonRepository extends CrudRepository<Person, Integer> {

    List<Person> findAllBy();
}
