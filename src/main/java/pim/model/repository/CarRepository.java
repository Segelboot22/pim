package pim.model.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import pim.model.entity.Car;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

public interface CarRepository extends CrudRepository<Car, Long> {

    List<Car> findAllBy();

    List<Car> findAllBy(Pageable pageable);

    @Modifying
    @Transactional
    @Query("DELETE FROM Car c WHERE c.date < ?1")
    void deleteAllByDateBefore(Date date);
}
