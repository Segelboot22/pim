package pim.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import pim.distributed.BeanRegistry;
import pim.distributed.DistributedBean;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.util.Date;

@Data
@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(BeanRegistry.class)
public class Car implements DistributedBean {

    @Id
    private Long id;

    @NotBlank
    private String brand;

    @Positive
    private int horsePower;

    @Positive
    private int topSpeed;

    @Positive
    private int fuelCapacity;

    private Date date;


}


