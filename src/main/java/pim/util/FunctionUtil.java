package pim.util;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public interface FunctionUtil {

    @FunctionalInterface
    interface CheckedFunction<T, R, EX extends Exception> {
        R apply(T element) throws EX;
    }

    @FunctionalInterface
    interface CheckedConsumer<T, EX extends Exception> {
        void accept(T t) throws EX;
    }

    @FunctionalInterface
    interface CheckedSupplier<R, EX extends Exception> {
        R get() throws EX;
    }

    static <R> Supplier<R> uncheckSupplier(CheckedSupplier<R, Exception> function) {
        return () -> {
            try {
                return function.get();
            } catch (Exception ex) {
                return sneakyThrow(ex);
            }
        };
    }

    static <T> Consumer<T> uncheckConsumer(CheckedConsumer<T, Exception> function) {
        return element -> {
            try {
                function.accept(element);
            } catch (Exception ex) {
                sneakyThrow(ex);
            }
        };
    }

    static <T, R> Function<T, R> uncheck(CheckedFunction<T, R, Exception> function) {
        return element -> {
            try {
                return function.apply(element);
            } catch (Exception ex) {
                return sneakyThrow(ex);
            }
        };
    }

    @SuppressWarnings("unchecked")
    static <E extends Throwable, T> T sneakyThrow(Throwable t) throws E {
        throw (E) t;
    }
}
