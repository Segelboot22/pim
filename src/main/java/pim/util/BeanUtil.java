package pim.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.support.Repositories;
import org.springframework.stereotype.Component;
import pim.distributed.DistributedBean;

import java.util.Optional;

@Component
public class BeanUtil implements ApplicationContextAware {

    private static ApplicationContext theApplicationContext;
    private static Repositories theRepositories;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        theApplicationContext = applicationContext;
        theRepositories = new Repositories(applicationContext);
    }

    public static Optional<CrudRepository> getRepositoryForEntity(Class<? extends DistributedBean> distributedBeanClass) {
        return theRepositories.getRepositoryFor(distributedBeanClass).map(CrudRepository.class::cast);
    }

    public static <T> T getBean(Class<T> clazz) {
        return theApplicationContext.getBean(clazz);
    }

    public static <T> T getBean(String clazzName, Class<T> clazz) {
        return theApplicationContext.getBean(clazzName, clazz);
    }

}
