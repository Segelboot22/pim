package pim.ui.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import pim.ui.component.view.icons.IconsView;
import pim.ui.component.view.tutorial.TutorialView;


@UIScope
@SpringComponent
public class SideMenu extends VerticalLayout {

    public SideMenu() {

        Icon image = new Icon(VaadinIcon.TOOTH);
        add(image);

        Button tutorial = addMenuElement(TutorialView.class, "Tutorial");
        tutorial.setIcon(new Icon(VaadinIcon.BOOK));
        add(tutorial);

        Button iconsView = addMenuElement(IconsView.class, "Icons");
        iconsView.setIcon(new Icon(VaadinIcon.ABACUS));
        add(iconsView);

        Button logout = new Button("Logout");
        logout.setIcon(new Icon(VaadinIcon.CLOSE));
        logout.addClickListener(event -> getUI().ifPresent(ui -> ui.getSession().close()));
        add(logout);

        setWidth("200px");
    }

    private Button addMenuElement(Class<? extends Component> navigationTarget, String name) {
        Button button = new Button(name);
        button.addClickListener(x -> getUI().ifPresent(ui -> ui.navigate(navigationTarget)));
        return button;
    }
}
