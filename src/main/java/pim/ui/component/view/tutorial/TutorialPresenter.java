package pim.ui.component.view.tutorial;

import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;
import pim.model.entity.Car;
import pim.model.entity.Person;
import pim.model.repository.CarRepository;
import pim.model.repository.PersonRepository;

import java.util.List;

import static java.util.stream.Collectors.joining;

@UIScope
@SpringComponent
class TutorialPresenter {

    private final PersonRepository personRepository;
    private final CarRepository carRepository;

    @Autowired
    public TutorialPresenter(PersonRepository personRepository, CarRepository carRepository) {
        this.personRepository = personRepository;
        this.carRepository = carRepository;
    }

    public Person findFirstPerson() {
        return personRepository.findById(1).get();
    }

    public void savePerson(Person person) {
        personRepository.save(person);
    }

    public Person findSecondPerson() {
        return personRepository.findById(2).get();
    }

    public String dumpDatabase() {
        return personRepository.findAllBy().stream().map(Person::toString).collect(joining(" "));
    }

    public List<Car> findAllCars() {
        return carRepository.findAllBy();
    }

    public Car findFirstCar() {
        return carRepository.findById(1L).orElse(null);
    }

    public void saveCar(Car bean) {
        carRepository.save(bean);
    }
}
