package pim.ui.component.view.tutorial;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import pim.distributed.BeanRegistry;
import pim.model.entity.Car;
import pim.model.entity.Person;
import pim.model.repository.CarRepository;
import pim.ui.MainLayout;

import javax.annotation.PostConstruct;
import java.util.List;

@Route(value = "tutorial", layout = MainLayout.class)
public class TutorialView extends Div {

    private final CarRepository carRepository;
    private final TutorialPresenter presenter;

    @Autowired
    public TutorialView(CarRepository carRepository, TutorialPresenter tutorialPresenter) {
        this.carRepository = carRepository;
        this.presenter = tutorialPresenter;
    }

    @PostConstruct
    public void init() {

        setSizeFull();

        Person matzeBean = presenter.findFirstPerson();

        TextField textField = new TextField();
        Binder<Person> personBinder = BeanRegistry.registerBean(matzeBean);
        personBinder.bind(textField, Person::getFirstName, Person::setFirstName);
        personBinder.addValueChangeListener(e -> presenter.savePerson(personBinder.getBean()));
        add(textField);


        Person benniBean = presenter.findSecondPerson();

        TextField textField1 = new TextField();
        Binder<Person> personBinder1 = BeanRegistry.registerBean(benniBean);
        personBinder1.bind(textField1, "firstName");
        personBinder1.addValueChangeListener(e -> presenter.savePerson(personBinder1.getBean()));
        add(textField1);


        Car car = presenter.findFirstCar();

        TextField textField2 = new TextField();
        Binder<Car> personBinder2 = BeanRegistry.registerBean(car);
        personBinder2.bind(textField2, "brand");
        personBinder2.addValueChangeListener(e -> presenter.saveCar(personBinder2.getBean()));
        add(textField2);

        add(new Span(" "));

        DataProvider<Car, Void> dataProvider = BeanRegistry.fromCallbacks(Car.class, query -> {
            int limit = query.getLimit();
            int offset = query.getOffset();
            return carRepository.findAllBy(PageRequest.of(offset, limit)).stream();
        }, query -> (int) carRepository.count());


        Grid<Car> carGrid = new Grid<>(Car.class);
        carGrid.getColumnByKey("id").setHeader("Id");
        carGrid.getColumnByKey("brand").setHeader("Marke");
        carGrid.getColumnByKey("horsePower").setHeader("PS");
        carGrid.getColumnByKey("topSpeed").setHeader("Höchstgeschwindigkeit");
        carGrid.getColumnByKey("fuelCapacity").setHeader("Tankinhalt");

        carGrid.setDataProvider(dataProvider);

        add(carGrid);
    }
}
