package pim.ui;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;
import pim.ui.component.SideMenu;


@Push
@UIScope
@SpringComponent
public class MainLayout extends HorizontalLayout implements RouterLayout {

    @Autowired
    public MainLayout(SideMenu sideMenu) {
        add(sideMenu);
    }
}