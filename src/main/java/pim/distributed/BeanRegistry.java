package pim.distributed;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.CallbackDataProvider;
import com.vaadin.flow.data.provider.DataProvider;
import lombok.SneakyThrows;
import lombok.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pim.util.BeanUtil;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;


@Service
@SuppressWarnings("unchecked")
public class BeanRegistry {

    private static final Logger logger = LoggerFactory.getLogger(BeanRegistry.class);
    private static final ConcurrentMap<MapKey, CopyOnWriteArrayList<Rebinder<? extends DistributedBean>>> rebindCache = new ConcurrentHashMap<>();
    private static final ConcurrentMap<Class<? extends DistributedBean>, CopyOnWriteArrayList<Refresher>> refreshCache = new ConcurrentHashMap<>();

    @Value
    private static class MapKey {
        private Serializable id;
        private Class clazz;
    }

    private static class Refresher {
        private final WeakReference<UI> uiWeakReference;
        private final WeakReference<DataProvider> dataProviderWeakReference;

        Refresher(UI uiWeakReference, DataProvider dataProviderWeakReference) {
            this.uiWeakReference = new WeakReference<>(uiWeakReference);
            this.dataProviderWeakReference = new WeakReference<>(dataProviderWeakReference);
        }

        boolean isOutdated() {
            var ui = uiWeakReference.get();
            var dataProvider = dataProviderWeakReference.get();

            return dataProvider == null || ui == null || ui.getSession() == null;
        }

        void refresh(DistributedBean distributedBean) {
            var ui = uiWeakReference.get();
            var dataProvider = dataProviderWeakReference.get();

            if (ui != null && dataProvider != null) {
                logger.info("---- Refresh of DistributedBean " + distributedBean + " ----");
                ui.access(() -> dataProvider.refreshItem(distributedBean));
            }
        }
    }

    private static class Rebinder<T extends DistributedBean> {
        private final WeakReference<Binder<T>> binderWeakReference;
        private final WeakReference<UI> uiWeakReference;

        Rebinder(Binder<T> binder, UI ui) {
            this.binderWeakReference = new WeakReference<>(binder);
            this.uiWeakReference = new WeakReference<>(ui);
        }

        boolean isOutdated() {
            var ui = uiWeakReference.get();
            var binder = binderWeakReference.get();

            return binder == null || ui == null || ui.getSession() == null;
        }

        void rebind(DistributedBean distributedBean) {
            var ui = uiWeakReference.get();
            var binder = binderWeakReference.get();

            if (ui != null && binder != null) {
                logger.info("---- Rebind of DistributedBean " + distributedBean + " ----");
                ui.access(() -> binder.setBean((T) distributedBean));
            }
        }
    }

    public static <T extends DistributedBean> BeanValidationBinder<T> registerValidatedBean(T bean) {
        BeanValidationBinder<T> binder = new BeanValidationBinder<>((Class<T>) bean.getClass());
        UI ui = UI.getCurrent();

        insertIntoRebindCache(bean, binder, ui);

        binder.setBean(bean);
        return binder;
    }

    public static <T extends DistributedBean> Binder<T> registerBean(T bean) {
        Binder<T> binder = new Binder<>((Class<T>) bean.getClass());
        UI ui = UI.getCurrent();

        insertIntoRebindCache(bean, binder, ui);

        binder.setBean(bean);
        return binder;
    }

    public static <T extends DistributedBean, F> DataProvider<T, F> fromCallbacks(Class<T> clazz, CallbackDataProvider.FetchCallback<T, F> fetchCallback, CallbackDataProvider.CountCallback<T, F> countCallback) {
        CallbackDataProvider<T, F> callbackDataProvider = new CallbackDataProvider(fetchCallback, countCallback, bean -> ((DistributedBean) bean).getId());
        UI ui = UI.getCurrent();

        insertIntoRefreshCache(clazz, callbackDataProvider, ui);

        return callbackDataProvider;
    }

    private static <T extends DistributedBean> void insertIntoRefreshCache(Class<T> clazz, DataProvider dataProvider, UI ui) {
        CopyOnWriteArrayList<Refresher> refreshers = refreshCache.getOrDefault(clazz, new CopyOnWriteArrayList<>());
        refreshers.removeIf(refresher -> refresher.isOutdated() || (refresher.dataProviderWeakReference.get() == dataProvider && refresher.uiWeakReference.get() == ui));
        refreshers.add(new Refresher(ui, dataProvider));
        refreshCache.put(clazz, refreshers);
    }

    private static <T extends DistributedBean> void insertIntoRebindCache(T bean, Binder<T> binder, UI ui) {
        var mapKey = new MapKey(bean.getId(), bean.getClass());

        var rebinders = rebindCache.getOrDefault(mapKey, new CopyOnWriteArrayList<>());
        rebinders.removeIf(rebinder -> rebinder.isOutdated() || (rebinder.binderWeakReference.get() == binder && rebinder.uiWeakReference.get() == ui));

        Rebinder<T> rebinder = new Rebinder<>(binder, ui);
        rebinders.add(rebinder);
        rebindCache.put(mapKey, rebinders);
    }

    private static <T extends DistributedBean> void triggerBeanRefresh(T distributedBean) {

        var mapKey = new MapKey(distributedBean.getId(), distributedBean.getClass());
        CopyOnWriteArrayList<Rebinder<? extends DistributedBean>> rebinders = rebindCache.get(mapKey);

        if (rebinders != null) {
            rebinders.removeIf(Rebinder::isOutdated);
            rebinders.forEach(rebinder -> rebinder.rebind(distributedBean));
        }

        CopyOnWriteArrayList<Refresher> refreshers = refreshCache.get(distributedBean.getClass());

        if (refreshers != null) {
            refreshers.removeIf(Refresher::isOutdated);
            refreshers.forEach(refresher -> refresher.refresh(distributedBean));
        }
    }

    @SneakyThrows
    void receiveBeanRefreshFromCluster(Class<? extends DistributedBean> beanClazz, Serializable id) {
        Thread.sleep(100);
        BeanUtil.getRepositoryForEntity(beanClazz)
                .flatMap(crudRepository -> crudRepository.findById(id))
                .ifPresent(o -> triggerBeanRefresh((DistributedBean) o));
    }

    @PreUpdate
    private void preUpdate(DistributedBean distributedBean) {
        logger.info("---- preUpdate: " + distributedBean.toString() + " ----");
        BeanUtil.getBean(ClusterService.class).sendClusterMessage(distributedBean);
    }

    @PrePersist
    private void prePersist(DistributedBean distributedBean) {
        logger.info("---- prePersist: " + distributedBean.toString() + " ----");
    }

}
