package pim.distributed;

import java.io.Serializable;

public interface DistributedBean {

    <T extends Serializable> T getId();

}
