package pim.distributed;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.jboss.logging.Logger;
import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.util.MessageBatch;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.Serializable;

@Service
public class ClusterService extends ReceiverAdapter {

    private static final Logger logger = Logger.getLogger(ClusterService.class);

    private final JChannel jChannel;
    private final BeanRegistry beanRegistry;

    public ClusterService(BeanRegistry beanRegistry) throws Exception {
        this.beanRegistry = beanRegistry;
        this.jChannel = new JChannel();
    }

    @PostConstruct
    private void init() throws Exception {
        jChannel.connect("pim_cluster");
        jChannel.setDiscardOwnMessages(false);
        jChannel.setReceiver(this);
    }

    void sendClusterMessage(DistributedBean distributedBean) {

        var distributedBeanMessage = new DistributedBeanMessage(distributedBean.getClass(), distributedBean.getId());

        try {
            jChannel.send(distributedBeanMessage);
        } catch (Exception e) {
            logger.error("Cannot send DistributedBean " + distributedBean, e);
        }
    }

    @Override
    public void receive(Message msg) {
        var distributedBeanMessage = (DistributedBeanMessage) msg;
        beanRegistry.receiveBeanRefreshFromCluster(distributedBeanMessage.beanClazz, distributedBeanMessage.beanId);
    }

    @Override
    public void receive(MessageBatch batch) {
        batch.stream()
                .map(DistributedBeanMessage.class::cast)
                .forEach(distributedBeanMessage -> beanRegistry.receiveBeanRefreshFromCluster(distributedBeanMessage.beanClazz, distributedBeanMessage.beanId));
    }


    @Value
    @EqualsAndHashCode(callSuper = false)
    private class DistributedBeanMessage extends Message {
        private Class<? extends DistributedBean> beanClazz;
        private Serializable beanId;
    }
}
