package pim.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pim.model.entity.Person;
import pim.service.TestService;

import java.util.List;

@RestController
public class EndPoint {

    private final TestService testService;

    @Autowired
    public EndPoint(TestService testService) {
        this.testService = testService;
    }

    @GetMapping(value = "/all")
    public List<Person> getAllUsers() {
        return testService.getSlow();
    }

}
