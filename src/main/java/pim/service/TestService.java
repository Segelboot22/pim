package pim.service;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import pim.model.entity.Person;
import pim.model.repository.PersonRepository;

import java.util.List;

@Service
@CacheConfig(cacheNames={"persons"})
public class TestService {

    private final PersonRepository personRepository;

    public TestService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Cacheable
    public List<Person> getSlow(){
        try {
            Thread.sleep(5_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return personRepository.findAllBy();
    }
}
